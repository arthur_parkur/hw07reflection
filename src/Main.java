import TestAnnatation.Test;

import java.lang.annotation.Annotation;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class Main {

    public static void main(String[] args) throws ClassNotFoundException, InstantiationException, IllegalAccessException, InvocationTargetException {
        start("Some");
    }

    public static void start(String className) throws ClassNotFoundException, IllegalAccessException, InstantiationException, InvocationTargetException {
        Class cl = Class.forName(className);

        Object inst = cl.newInstance();

        int beforeSuitCount = 0;
        int afterSuitCount = 0;

        Method[] methods = cl.getDeclaredMethods();
        for (Method method : methods) {
            Annotation[] anno = method.getAnnotations();
            for(Annotation annotation : anno) {
                if((annotation.toString()).equals("@TestAnnatation.AfterSuite()")) {
                    ++afterSuitCount;
                }
                if((annotation.toString()).equals("@TestAnnatation.BeforeSuite()")) {
                    ++beforeSuitCount;
                }
            }
        }
        if(beforeSuitCount != 1 || afterSuitCount != 1) throw new RuntimeException();

        //run beforeSuite
        for (Method method : methods) {
            Annotation[] anno = method.getAnnotations();
            for(Annotation annotation : anno) {
                if((annotation.toString()).equals("@TestAnnatation.BeforeSuite()")) {
                    method.invoke(inst);
                }
            }
        }
        //run Test methods
        for (int i = 1; i <= 10; i++) {
            for (Method method : methods) {
                Test test = method.getAnnotation(Test.class);
                if(test != null) {
                    if (test.order() == i) method.invoke(inst);
                    --i;
                    continue;
                }
            }
        }
        //run afterSuite
        for (Method method : methods) {
            Annotation[] anno = method.getAnnotations();
            for(Annotation annotation : anno) {
                if((annotation.toString()).equals("@TestAnnatation.AfterSuite()")) {
                    method.invoke(inst);
                }
            }
        }
    }

    public static void start(Class clss) throws ClassNotFoundException, IllegalAccessException, InstantiationException, InvocationTargetException {
        Class cl = clss;

        Object inst = cl.newInstance();

        int beforeSuitCount = 0;
        int afterSuitCount = 0;

        Method[] methods = cl.getDeclaredMethods();
        for (Method method : methods) {
            Annotation[] anno = method.getAnnotations();
            for(Annotation annotation : anno) {
                if((annotation.toString()).equals("@TestAnnatation.AfterSuite()")) {
                    ++afterSuitCount;
                }
                if((annotation.toString()).equals("@TestAnnatation.BeforeSuite()")) {
                    ++beforeSuitCount;
                }
            }
        }
        if(beforeSuitCount != 1 || afterSuitCount != 1) throw new RuntimeException();

        //run beforeSuite
        for (Method method : methods) {
            Annotation[] anno = method.getAnnotations();
            for(Annotation annotation : anno) {
                if((annotation.toString()).equals("@TestAnnatation.BeforeSuite()")) {
                    method.invoke(inst);
                }
            }
        }
        //run Test methods
        for (int i = 1; i <= 10; i++) {
            for (Method method : methods) {
                Test test = method.getAnnotation(Test.class);
                if(test != null) {
                    if (test.order() == i) method.invoke(inst);
                    --i;
                    continue;
                }
            }
        }
        //run afterSuite
        for (Method method : methods) {
            Annotation[] anno = method.getAnnotations();
            for(Annotation annotation : anno) {
                if((annotation.toString()).equals("@TestAnnatation.AfterSuite()")) {
                    method.invoke(inst);
                }
            }
        }
    }
}
